# Project <ds_name>

Cpp project

A multiple vacuum classes server.


## Cloning

```
git clone -b release git@gitlab.esrf.fr:accelerators/.....git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher
* omniORB release 4 or higher
* libzmq
...


#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.16 or greater is required to perform the build. 


### Build

Instructions on building the project.

CMake example: 

```bash
cd project_name
cmake -S . -B build
cmake --build build --parallel $(nproc)
cmake --install build --prefix build/install
tree --du -h -C build/install
```

CMake example at ESRF: 
with TANGO_VERSION in [ "tango-9.3", "tango-9.4", "tango-9.5" ]

```bash
cd project_name
cmake -S . -B build -DUSER_CONFIG=/opt/os_dev/cpp/<TANGO_VERSION>.cmake 
cmake --build build --parallel $(nproc)
cmake --install build --prefix build/install
tree --du -h -C build/install
```


