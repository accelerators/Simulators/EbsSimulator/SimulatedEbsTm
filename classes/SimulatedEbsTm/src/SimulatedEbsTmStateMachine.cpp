/*----- PROTECTED REGION ID(SimulatedEbsTmStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        SimulatedEbsTmStateMachine.cpp
//
// description : State machine file for the SimulatedEbsTm class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

/*----- PROTECTED REGION END -----*/	//	SimulatedEbsTm::SimulatedEbsTmStateMachine.cpp
#include <tango/classes/SimulatedEbsTm/SimulatedEbsTm.h>

//================================================================
//  States  |  Description
//================================================================
//  ON      |  
//  FAULT   |  
//  OFF     |  


namespace SimulatedEbsTm_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsTm::is_Reset_allowed()
 * Description:  Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool SimulatedEbsTm::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::ON ||
		get_state()==Tango::OFF)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsTm::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsTm::ResetStateAllowed
		return false;
	}
	return true;
}


/*----- PROTECTED REGION ID(SimulatedEbsTm::SimulatedEbsTmStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	SimulatedEbsTm::SimulatedEbsTmStateAllowed.AdditionalMethods

}	//	End of namespace
